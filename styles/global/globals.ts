'use client';

import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
      *,
      *::before,
      *::after{
          padding: 2rem;
          margin: 0;
          box-sizing: border-box;
      }

      body{
          height: 100vh;
          width: 100vw;
          overflow-x: hidden;        
          
      }

  `;
