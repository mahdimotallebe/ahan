import { GlobalStyle } from '../../styles/global/globals';

interface Props {
  children: React.ReactNode;
}

const RootLayout = ({ children }: Props) => {
  return (
    <html>
      <head />
      <body>
        <GlobalStyle />
        {children}
      </body>
    </html>
  );
};

export default RootLayout;
