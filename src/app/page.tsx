'use client';
import Link from 'next/link';
import React from 'react';
import { GlobalStyle } from '../../styles/global/globals';

const Home = () => {
  return (
    <>
      <Link href="/client">Client fetching</Link>
      <Link href="/ssr?url=https://jsonplaceholder.typicode.com/users">
        SSR fetching
      </Link>
    </>
  );
};

export default Home;
