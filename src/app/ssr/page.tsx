import React from 'react';
import axios from 'axios';

interface Props {
  searchParams?: { url: string };
}

//fetching data
const getData = async (url: string | undefined) => {
  try {
    if (url !== undefined) {
      const res = await axios(url);
      const data = await res.data;

      return data;
    }
  } catch (e) {
    console.log(e);
  }
};
const SSR = async ({ searchParams }: Props) => {
  const data = await getData(searchParams?.url);
  return <div>{!data ? <div>fetching data</div> : JSON.stringify(data)}</div>;
};

export default SSR;
