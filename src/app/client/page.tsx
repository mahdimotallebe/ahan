'use client';
import React from 'react';

//components
import { useFetch } from '../../hook/useFetch';

//styles
import styled from 'styled-components';

const Input = styled.input`
  width: 60%;
  border: none;
  background: #eee;
  padding: 1.5rem;
  color: #888;
`;
const Button = styled.button`
  border: none;
  background: #000;
  color: #fff;
  padding: 1.5rem;
  text-transform: capitalize;
  cursor: pointer;
`;
const Client = () => {
  const [url, setUrl] = React.useState<string>(
    'https://jsonplaceholder.typicode.com/users'
  );
  const { loading, getData, data } = useFetch(url);

  const handleUrl = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUrl(e.target.value);
  };
  return (
    <>
      {loading ? (
        <div>fetching data...</div>
      ) : (
        <>
          <Input type="text" value={url} onChange={(e) => handleUrl(e)} />
          <Button onClick={() => getData()}>getData</Button>
          <div>{JSON.stringify(data)}</div>
        </>
      )}
    </>
  );
};

export default Client;
