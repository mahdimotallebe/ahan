import axios from 'axios';
import React from 'react';

export const useFetch = (api: string) => {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [data, setData] = React.useState();
  const getData = async () => {
    try {
      setLoading(true);
      const res = await axios(api);
      const resData: any = await res.data;
      setData(resData);
      setLoading(false);
    } catch (e) {
      console.log(e);
    }
  };
  return {
    loading,
    getData,
    data
  };
};
